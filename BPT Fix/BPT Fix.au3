#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=.\BPT Fix.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Fix para problemas de espanhol em arquivos de template (BPT) do Citadon.
#AutoIt3Wrapper_Res_Description=CW BPT Fix
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2012 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|BPT Fix
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_AU3Check=n
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\"
#AutoIt3Wrapper_Run_Tidy=y
#Tidy_Parameters=/rel
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <FileConstants.au3>
#include <File.au3>
#include <AU3Text\AU3Text.au3>
$app = "BPT Fix"
$appFileName = StringRegExpReplace(@ScriptName, '\.(?i)(exe|au3|a3x)$', '')
If @Compiled Then
	Global $AU3TEXT_I18NDIR = @TempDir
	FileInstall("I18n\BPT Fix.pt.lng", @TempDir & "\" & $appFileName & ".pt.lng", $FC_OVERWRITE)
	FileInstall("I18n\BPT Fix.es.lng", @TempDir & "\" & $appFileName & ".es.lng", $FC_OVERWRITE)
EndIf
_AU3Text_BindTextSection()
$filename = FileOpenDialog(_S("Select a template file to fix", "File Dialog", "Message"), @DesktopDir, _S("Business Process Template (*.bpt)", "File Dialog", "Filter"), 1)
; No file...
If @error Then
	MsgBox(4096, $app, _S("No file selected!", "Messages", "No File"))
Else
	; New file name
	$newfilename = StringRegExpReplace($filename, '\.(?i)(bpt)$', '') & " fixed.bpt"
	; Open files
	$filehandler = FileOpen($filename)
	$newfilehandler = FileOpen($newfilename, 2)
	; Read and close the current
	$filetext = FileRead($filehandler)
	FileClose($filehandler)
	; Makes the needed replaces
	$filetext = StringReplace($filetext, "Falso", "False")
	$filetext = StringReplace($filetext, "Verdadero", "True")
	$filetext = StringRegExpReplace($filetext, "[àáãäâ]", "a")
	$filetext = StringRegExpReplace($filetext, "[ÀÁÃÄÂ]", "A")
	$filetext = StringRegExpReplace($filetext, "[èéëê]", "e")
	$filetext = StringRegExpReplace($filetext, "[ÈÉËÊ]", "E")
	$filetext = StringRegExpReplace($filetext, "[ìíïî]", "i")
	$filetext = StringRegExpReplace($filetext, "[ÌÍÏÎ]", "I")
	$filetext = StringRegExpReplace($filetext, "[òóõöô]", "o")
	$filetext = StringRegExpReplace($filetext, "[ÒÓÕÖÔ]", "O")
	$filetext = StringRegExpReplace($filetext, "[úùüû]", "u")
	$filetext = StringRegExpReplace($filetext, "[ÚÙÜÛ]", "U")
	$filetext = StringRegExpReplace($filetext, "[ç]", "c")
	$filetext = StringRegExpReplace($filetext, "[Ç]", "C")
	$filetext = StringRegExpReplace($filetext, "[ñ]", "n")
	$filetext = StringRegExpReplace($filetext, "[Ñ]", "N")
	$filetext = StringRegExpReplace($filetext, "[\x0-\x9\xB\xC\xE-\x1F\x7F-\xFF]", "")
	; Write down to the new file
	FileWrite($newfilehandler, $filetext)
	FileClose($newfilehandler)
	MsgBox(4096, "BPT Fix", StringFormat(_S("Success!" & @CRLF & @CRLF & "File ""%s"" generated!", "Messages", "Success"), StringRegExp($newfilename, '.*\\(.*)', 1)[0]))
EndIf
FileDelete(@TempDir & "\" & $appFileName & ".*.lng")
