#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=File_DLL.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Fix para problemas de InstallShield referentes ao setup do Citadon.
#AutoIt3Wrapper_Res_Description=InstallShield Fix
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|InstallShield Fix
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
Dim $title = "InstallShield Fix"
ProgressOn($title, "Checking administrative privileges...")
If Not IsAdmin() Then
	MsgBox(16, $title, "This fix must be run with administrative privileges!")
	Exit
EndIf
Sleep(200)
ProgressSet(0, "Checking platform...")
Local $path
If @OSArch == "X86" Then
	$path = """%COMMONPROGRAMFILES%\InstallShield\Driver\8"""
Else
	$path = """%COMMONPROGRAMFILES(x86)%\InstallShield\Driver\8"""
EndIf
Sleep(200)
ProgressSet(17, "Security cleanup...")
RunWait(@ComSpec & " /c " & "del /F " & $path, @SW_HIDE)
Sleep(200)
ProgressSet(34, "Unpacking files...")
FileInstall(".\ISScript8.Msi", @TempDir)
Sleep(200)
ProgressSet(51, "Installing...")
RunWait("ISScript8.Msi", @TempDir)
Sleep(200)
ProgressSet(68, "Registering services...")
RunWait(@ComSpec & " /c " & $path & "\Intel 32\IDriver.exe /REGSERVER", @SW_HIDE)
RunWait(@ComSpec & " /c " & "%WINDIR%\System32\msiexec.exe /REGSERVER", @SW_HIDE)
Sleep(200)
ProgressSet(85, "Cleaning up temporary files...")
FileDelete(@TempDir & "\ISScript8.Msi")
Sleep(200)
ProgressSet(100, " ", "Done!")
Sleep(500)
ProgressOff()
MsgBox(0, $title, "Intalled with success!")
