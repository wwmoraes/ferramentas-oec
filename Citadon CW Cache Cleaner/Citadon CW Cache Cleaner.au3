#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=.\CitadonCW.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Limpador de cache para o cliente do Citadon CW.
#AutoIt3Wrapper_Res_Description=CW Cache Cleaner
#AutoIt3Wrapper_Res_Fileversion=1.0.0.1
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Citadon CW Cache Cleaner
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <File.au3>

Global $cacheBasePath = @ProgramFilesDir & "\Citadon\CitadonCW\cache\"
Global $separator = "|"
Opt("GUIDataSeparatorChar", $separator)

#region ### START Koda GUI section ### Form=
$CCWCacheCleaner = GUICreate("Citadon CW Cache Cleaner", 243, 59, 191, 121)
$UserLbl = GUICtrlCreateLabel("Informe o nome do usuário a se limpar o cache", 8, 8, 229, 16)
;$LoginInput = GUICtrlCreateInput("Login ID", 8, 24, 137, 21)
$LoginInput = GUICtrlCreateCombo("", 8, 24, 137, 21)
$OkBtn = GUICtrlCreateButton("OK", 152, 24, 81, 25)
#endregion ### END Koda GUI section ###

$loginIDs = getLoginIDs()
GUICtrlSetData($LoginInput, _ArrayToString($loginIDs, $separator), $loginIDs[0])
GUISetState(@SW_SHOW)

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
		Case $OkBtn
			GUISetState(@SW_HIDE)
			$cachepath = $cacheBasePath & GUICtrlRead($LoginInput)
			;$dir = ShellExecute("dir.exe","",$cachepath)
			;MsgBox(64,"Path",$dir)
			If FileExists($cachepath) Then
				DirRemove($cachepath, 1)
				MsgBox(64, "Sucesso", "Cache removido com sucesso!")
				$loginIDs = getLoginIDs()
				GUICtrlSetData($LoginInput, _ArrayToString($loginIDs, $separator), $loginIDs[0])
			Else
				MsgBox(16, "Erro", "Não existe cache para o usuário indicado!")
			EndIf
			GUISetState(@SW_SHOW)
			;Exit
	EndSwitch
WEnd

Func getLoginIDs()
   $aPaths = _FileListToArray(@ProgramFilesDir & "\Citadon\CitadonCW\cache\", "*", $FLTA_FOLDERS)

	Switch @error
		Case 0
		   ;return $separator & _ArrayToString(_ArrayExtract($aPaths,1,$aPaths[0]), $separator)
		   return _ArrayExtract($aPaths,1,$aPaths[0])
		Case 1
			MsgBox(16, "Erro", "Pasta de cache não encontrada ou inválida.")
			Exit
		Case 4
			MsgBox(64, "Sucesso", "Não há cache para remover!")
			Exit
	EndSwitch
EndFunc