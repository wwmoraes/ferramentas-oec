#NoTrayIcon
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=pc_info.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Informa dados básicos do computador.
#AutoIt3Wrapper_Res_Description=PC Info
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|PC Info
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

$appName = "PC Info"

;==============================================================================
; Functions
;==============================================================================
Func ExitMsg($msg, $code = 0)
	$boxType = 64
	If $code <> 0 Then $boxType = 16
	MsgBox($boxType, $appName, $msg)
	Exit $code
EndFunc   ;==>ExitMsg
;==============================================================================
; Main Code
;==============================================================================
; Get the WMI COM object
$objWMIService = ObjGet("winmgmts:\\" & @ComputerName & "\root\CIMV2")
If Not IsObj($objWMIService) Or @error <> 0 Then ExitMsg("Couldn't get the COM object!", 1)
; Query the data we are looking for
$colItems = $objWMIService.ExecQuery("SELECT Manufacturer,Model,Name,SystemType FROM Win32_ComputerSystem", "WQL", 0x30)
If Not IsObj($colItems) Then ExitMsg("Couldn't query or no information returned!", 1)
; Extract to local variables
Dim $manufacturer, $model, $name, $systemType, $serialNumber
For $objItem In $colItems
	$manufacturer = $objItem.Manufacturer
	$model = $objItem.Model
	$name = $objItem.Name
	$systemType = $objItem.SystemType
Next
; Free the objects early - ah, the old but gold C virtues...
$colItems = 0
$objWMIService = 0
; Yey! Output the service tag in a very stylish and uncommon information box, and asks if the user want to copy it to the clipboard
$opt = -1
$text = "Manufacturer: "&$manufacturer&@CRLF&"Model: "&$model&@CRLF&"Name: "&$name&@CRLF&"System Type: "&$systemType
$opt = MsgBox(68, $appName, $text & @CRLF & @CRLF & "Do you want to copy it to the clipboard?")
If $opt == 6 Then ; Yes, copy please!
	ClipPut($text) ; Copying...
EndIf
