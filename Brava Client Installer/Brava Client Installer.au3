#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=BCDownload.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Instalador do controle IGC Brava Client necessário para visualizar documentos no Citadon.
#AutoIt3Wrapper_Res_Description=Brava Client Installer
#AutoIt3Wrapper_Res_Fileversion=2.3.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Brava Client Installer
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


#include <MsgBoxConstants.au3>
Local $title = "Brava Client Installer"

If Not IsAdmin() Then
	MsgBox($MB_ICONERROR + $MB_OK, $title, "This installer must ran with administrator rights/elevation.")
	Exit 1
EndIf

#include <InetConstants.au3>
#include <AutoItConstants.au3>
#include <File.au3>
#include "FileListToArray3.au3"

Local $url = "https://citadoncw.citadon.com/IGC/BravaClientXWrapper.cab"
Local $file = "BravaClientXWrapper.cab"
Local $progressPhases = 6 ; setup, download, unzip, purge old, install, clean-up

ProgressOn($title, "Iniciando...", "", -1, -1, $DLG_MOVEABLE)

Local $localfile = @TempDir & "\" & $file
Local $drive, $directory, $filename, $extension
_PathSplit($localfile, $drive, $directory, $filename, $extension)
Local $inffile = $drive & $directory & $filename & "\" & $filename & ".inf"
Local $cabdir = $drive & $directory & $filename & "\"

ProgressSetPhase(1, $progressPhases, 0, "Preparando instalação...")
DirRemove($cabdir, 1)
FileDelete($localfile)

Local $dl = InetGet($url, $localfile, $INET_FORCERELOAD, $INET_DOWNLOADBACKGROUND)
Local $dlnow = InetGetInfo($dl, $INET_DOWNLOADREAD)

Do
	Sleep(250)
	$dlnow = InetGetInfo($dl, $INET_DOWNLOADREAD)
	ProgressSetPhase(2, $progressPhases, $dlnow/InetGetInfo($dl, $INET_DOWNLOADSIZE)*100, _ByteSuffix($dlnow), "Baixando...")
Until InetGetInfo($dl, $INET_DOWNLOADCOMPLETE)
InetClose($dl)

ProgressSetPhase(3, $progressPhases, 50, "Por favor aguarde...", "Descompactando...")
;WindowsUnZip($localfile, $cabdir)
If Not FileExists($cabdir) Then DirCreate($cabdir)
;RunWait(@ComSpec & " /c " & "expand """ & $localfile & """ -f:* """ & $cabdir & """", $cabdir, @SW_HIDE)
RunWait(@ComSpec & " /c " & "extrac32 /Y /A /E /L """ & $cabdir & """ """ & $localfile & """", $cabdir, @SW_HIDE)

ProgressSetPhase(4, $progressPhases, 0, "Por favor aguarde...", "Desregistrando DLLs antigas...")
Local $dllfiles = _FileListToArray3(@UserProfileDir & "\IGC", "*.dll", 1, 1, 1, "msvc*.dll")
If Not @error Then
	For	$i = 1 To $dllfiles[0]
		ShellExecuteWait("regsvr32", "/U /S """ & $dllfiles[$i] & """")
		ProgressSetPhase(4, $progressPhases, 50*$i/$dllfiles[0], "Por favor aguarde...", "Desregistrando " & $dllfiles[$i] & "...")
	Next
EndIf
ProgressSetPhase(4, $progressPhases, 50, "Por favor aguarde...", "Removendo arquivos antigos...")
DirRemove(@UserProfileDir & "\IGC", 1)

ProgressSetPhase(5, $progressPhases, 0, "Por favor aguarde...", "Instalando...")
ShellExecuteWait("rundll32", "setupapi.dll,InstallHinfSection DefaultInstall 128 " & $inffile, $cabdir, $SHEX_OPEN, @SW_HIDE)

ProgressSetPhase(6, $progressPhases, 0, "Por favor aguarde...", "Limpando...")
DirRemove($cabdir, 1)

ProgressSetPhase(6, $progressPhases, 50, "Por favor aguarde...", "Limpando...")
FileDelete($localfile)

ProgressSetPhase(6, $progressPhases, 100, ":)", "Concluído!")
Sleep(500)
ProgressOff()
MsgBox(0, $title, "Instalação concluída.")

Func WindowsUnZip($sFileSource, $sFileDest, $Options = 20)
	$ShellApp = ObjCreate("Shell.Application")
	If (($sFileSource = "") Or ($sFileDest = "")) Then
		Return False
	EndIf
	If Not FileExists($sFileDest) Or Not StringInStr(FileGetAttrib($sFileDest), "D") Then
		DirCreate($sFileDest)
	EndIf
	$ShellApp.NameSpace($sFileDest).CopyHere($ShellApp.NameSpace($sFileSource).Items, $Options)
	Return True
EndFunc   ;==>WindowsUnZip

Func ProgressSetPhase($phase, $maxphase, $percent, $subtext = "", $maintext = $title)
	; Formula: phase * percent / maxphase
	; 100% = ($maxphase * 100)/$maxphase => 100%
	; first phase completed = 100/$maxphase => total % of first phase (if 5 phases, them 20%)
	; second phase completed = 200/$maxphase => total % of second phase (if 5 phases, them 40%)
	ProgressSet((($phase*$percent)/$maxphase),"[" & $phase & "/" & $maxphase & "] " & $subtext, $maintext)
EndFunc

Func _ByteSuffix($iBytes, $iRound = 2) ; By Spiff59
    Local $A, $aArray[9] = [" B", " KB", " MB", " GB", " TB", " PB", "EB", "ZB", "YB"]
    While $iBytes > 1023
        $A += 1
        $iBytes /= 1024
    WEnd
    Return Round($iBytes, $iRound) & $aArray[$A]
EndFunc   ;==>_ByteSuffix