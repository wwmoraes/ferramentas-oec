#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=File_DLL.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Instalador do controle File Upload necessário para o envio de múltiplos documentos ao Citadon.
#AutoIt3Wrapper_Res_Description=CW Multi Upload Installer
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Multi Upload Installer
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
Dim $InstDir = @WindowsDir & "\Downloaded Program Files\"
FileDelete($InstDir & "FileUpload.ocx")
FileDelete($InstDir & "ProcessCompound.exe")
FileDelete($InstDir & "DWGXrefListCollect.dll")
FileDelete($InstDir & "DGNXrefListCollect.dll")
FileDelete($InstDir & "CheckoutLocations.xml")
; TODO: list CONFLICT.* folders to remove recursively
FileInstall(".\FileUpload.ocx", $InstDir)
FileInstall(".\ProcessCompound.exe", $InstDir)
FileInstall(".\DWGXrefListCollect.dll", $InstDir)
FileInstall(".\DGNXrefListCollect.dll", $InstDir)
FileInstall(".\CheckoutLocations.xml", $InstDir)
MsgBox(0, "Multi Upload Install", "Intalled with success!")
