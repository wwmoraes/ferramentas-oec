#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=ntrtscan.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Executável postiço para suprir a existência do processo ntrtscan.exe, checado pelo Juniper para se ter acesso à VPN.
#AutoIt3Wrapper_Res_Description=Trend-Micro Realtime Scan Bypass
#AutoIt3Wrapper_Res_Fileversion=1.1.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Trend-Micro Realtime Scan Bypass
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Constants.au3>

Opt("TrayAutoPause", 0)
Opt("TrayMenuMode", 3)

Local $title = "Trend-Micro Realtime Scan Bypass"

TraySetIcon("ntrtscan.ico", 0)
Local $titleItem = TrayCreateItem($title)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("")
Local $siteItem = TrayCreateItem("My Blog")
Local $projItem = TrayCreateItem("My Projects")
Local $emailItem = TrayCreateItem("Email me")
TrayCreateItem("")
Local $exitItem = TrayCreateItem("Exit")
TraySetToolTip($title)

While 1
	$msg = TrayGetMsg()
	Switch $msg
		Case $siteItem
			ShellExecute("http://wwmoraes.com/")
		Case $projItem
			ShellExecute("http://git.wwmoraes.com/")
		Case $emailItem
			ShellExecute("http://scr.im/wwmoraes")
		Case $exitItem
			Exit
	EndSwitch
WEnd