#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=play upk.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Interface para utilizar os atalhos enviados pelos apresentadores de slide ao UPK.
#AutoIt3Wrapper_Res_Description=UPK Presenter
#AutoIt3Wrapper_Res_Fileversion=1.2.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|UPK Presenter
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <Constants.au3>
#include <MsgBoxConstants.au3>
#include <IE.au3>

Opt("TrayAutoPause", 0)
Opt("TrayMenuMode", 3)

$title = "UPK Presenter"

HotKeySet("{PGDN}", "PresenterNext") ; Next
HotKeySet("{PGUP}", "PresenterPrevious") ; Previous
HotKeySet("{ESC}", "PresenterEscape") ; Esc
HotKeySet("+{F5}", "PresenterEnter2") ; Play targus
HotKeySet("{F5}", "PresenterEnter") ; Play
HotKeySet("b", "PresenterShow") ; Show/Hide
HotKeySet("^{F12}", "CloseScript")

Func CloseScript()
	If MsgBox($MB_YESNO, $title, "Exit?") = $IDYES Then
		Exit
	EndIf
EndFunc   ;==>CloseScript

Func PresenterNext()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			Send("{DOWN}")
		Else ; Slides UPK (talvez...)
			Send("{ENTER}")
		EndIf
	Else
		HotKeySet("{PGDN}")
		Send("{PGDN}")
		HotKeySet("{PGDN}", "PresenterNext")
	EndIf
EndFunc   ;==>PresenterNext

Func PresenterPrevious()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			Send("{UP}")
		Else ; Slides UPK
			$oIE = _IEAttach($hwnd, "embedded")
			$oIE.window.OnActionSelection('Prev');
		EndIf
	Else
		HotKeySet("{PGUP}")
		Send("{PGUP}")
		HotKeySet("{PGUP}", "PresenterPrevious")
	EndIf
EndFunc   ;==>PresenterPrevious

Func PresenterEscape()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			$oIE = _IEAttach($hwnd, "embedded")
			Send("{LEFT}")
			$oIE.document.getElementById('mytreeframe').contentWindow.document.body.children(0).focus();
		EndIf
	Else
		HotKeySet("{ESC}")
		Send("{ESC}")
		HotKeySet("{ESC}", "PresenterEscape")
	EndIf
EndFunc   ;==>PresenterEscape

Func PresenterEnter()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			$oIE = _IEAttach($hwnd, "embedded")
			Send("{RIGHT}")
			$oIE.document.getElementById('mytreeframe').contentWindow.document.body.children(0).focus();
		EndIf
	Else
		HotKeySet("{F5}")
		Send("{F5}")
		HotKeySet("{F5}", "PresenterEnter")
	EndIf
EndFunc   ;==>PresenterEnter

Func PresenterEnter2()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			$oIE = _IEAttach($hwnd, "embedded")
			Send("{RIGHT}")
			$oIE.document.getElementById('mytreeframe').contentWindow.document.body.children(0).focus();
		EndIf
	Else
		HotKeySet("+{F5}")
		Send("+{F5}")
		HotKeySet("+{F5}", "PresenterEnter2")
	EndIf
EndFunc   ;==>PresenterEnter

Func PresenterShow()
	If WinActive("[CLASS:IEFrame]") Then
		$hwnd = WinGetHandle("")
		If StringInStr(WinGetTitle($hwnd), "UPK") Then ; Menu UPK
			$oIE = _IEAttach($hwnd, "embedded")
			Send("{ENTER}")
			$oIE.document.getElementById('mytreeframe').contentWindow.document.body.children(0).focus();
		Else ; Slides UPK
			HotKeySet("{ESC}")
			Send("{ESC}")
			HotKeySet("{ESC}", "PresenterEscape")
		EndIf
	Else
		HotKeySet("b")
		Send("b")
		HotKeySet("b", "PresenterShow")
	EndIf
EndFunc   ;==>PresenterShow

TraySetIcon("play upk.ico", 0)
TrayCreateItem($title)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("")
Local $siteItem = TrayCreateItem("My Blog")
Local $projItem = TrayCreateItem("My Projects")
Local $emailItem = TrayCreateItem("Email me")
TrayCreateItem("")
Local $exitItem = TrayCreateItem("Exit (Ctrl+F12)")
TraySetToolTip($title)
TrayTip($title, "Running! Press Ctrl+F12 to close or right click this icon and choose Exit.", 3, 1)

While 1
	$msg = TrayGetMsg()
	Switch $msg
		Case $siteItem
			ShellExecute("http://wwmoraes.com/")
		Case $projItem
			ShellExecute("http://git.wwmoraes.com/")
		Case $emailItem
			ShellExecute("http://scr.im/wwmoraes")
		Case $exitItem
			CloseScript()
	EndSwitch
WEnd
