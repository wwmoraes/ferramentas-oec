#region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=Net_Info.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Comment=Informa dados das conex�es de rede ativas.
#AutoIt3Wrapper_Res_Description=NetInfo
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#endregion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <MsgBoxConstants.au3>
#include <ButtonConstants.au3>
#include <ComboConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Array.au3>

Dim $appTitle = "Net Info"
Dim $WMI = ObjGet("winmgmts:\\" & @ComputerName & "\root\CIMV2")
Dim $Nads = $WMI.ExecQuery("Select * from Win32_NetworkAdapter where physicaladapter=true and netconnectionstatus=2")
Dim $Nad
Dim $Conf
Dim $msg

If $Nads.Count > 1 Then
	Dim $options[$Nads.Count]
	For $Nad in $Nads
		_ArrayAdd($options, $Nad.NetConnectionID)
	Next
	$Form = GUICreate($appTitle, 467, 67)
	$Label = GUICtrlCreateLabel("Adapter: ", 8, 8, 40, 17)
	$Combo = GUICtrlCreateCombo("", 55, 6, 400, 25, BitOR($CBS_DROPDOWN,$CBS_AUTOHSCROLL))
	GUICtrlSetData(-1, _ArrayToString($options, "|"))
	$Button = GUICtrlCreateButton("OK", 8, 32, 449, 25)
	GUISetState(@SW_SHOW)

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $GUI_EVENT_CLOSE
				Exit
			Case $Button
				Dim $sel = GUICtrlRead($Combo)
				If $sel = "" Then
					MsgBox($MB_APPLMODAL+$MB_ICONERROR, $appTitle, "Invalid selection.")
				Else
					$sel = _ArraySearch($options, $sel) - 1
					$Nad = $Nads.ItemIndex($sel)
					GUISetState(@SW_HIDE)
					ExitLoop
				EndIf
		EndSwitch
	WEnd
Else
	$Nad = $Nads.ItemIndex(0)
EndIf

If $Nad.MACAddress <> Null then
	$Confs = $WMI.ExecQuery("Select * from Win32_NetworkAdapterConfiguration where InterfaceIndex='" & $Nad.InterfaceIndex & "'")
	$msg = "Mac: " & $Nad.MACAddress
	$msg &= @CRLF & "DHCP: " & $Confs.ItemIndex(0).DHCPServer
	$msg &= @CRLF & "IPv4: " & $Confs.ItemIndex(0).IPAddress[0]
	$msg &= @CRLF & "IPv6: " & $Confs.ItemIndex(0).IPAddress[1]
	If MsgBox($MB_APPLMODAL+$MB_ICONQUESTION+$MB_YESNO, "NetInfo", $Nad.NetConnectionID & @CRLF & @CRLF & $msg & @CRLF & @CRLF & "Copy ro clipboard?") = $IDYES Then
		ClipPut($msg)
	endif
EndIf