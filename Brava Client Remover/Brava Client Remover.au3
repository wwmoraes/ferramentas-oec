#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=File_DLL.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Removedor do controle Brava Viewer do Citadon.
#AutoIt3Wrapper_Res_Description=Brava Client Remover
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Brava Client Remover
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****


#include <MsgBoxConstants.au3>
Local $title = "Brava Client Uninstall"

If Not IsAdmin() Then
	MsgBox($MB_ICONERROR + $MB_OK, $title, "This uninstaller must ran with administrator rights/elevation.")
	Exit 1
EndIf

Local $paths[2]

$paths[0] = @HomeDrive & "\x7_0\" ; Wrong installation path - occurs when the old .bat is used
$paths[1] = @HomeDrive & @HomePath & "\IGC\x7_0\"

For $path In $paths
	ShellExecuteWait("regsvr32", "/s /u BravaClientX.dll", $path)
	ShellExecuteWait("regsvr32", "/s /u BravaClientXWrapper.dll", $path)
	DirRemove($path, 1)
Next

MsgBox(0, $title, "Uninstalled with success!")
