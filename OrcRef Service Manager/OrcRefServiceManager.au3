#NoTrayIcon
#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=orcref.ico
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Inicia/pára os serviços necessários pelo OrcRef.
#AutoIt3Wrapper_Res_Description=OrcRef Service Manager
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|OrcRef Service Manager
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
Dim $title = "OrcRef Startup"
Dim $app = "orcref.exe"
Dim $services[2] = ["OracleServiceXE", "OracleXETNSListener"]

;===============================================================================
; DO NOT EDIT BELOW FOR YOUR OWN SAKE!
;===============================================================================
#include <Debug.au3>
;_DebugSetup("ServiceManager")
$needStartStop = false
Dim $states[UBound($services)]
For $i = 0 To UBound($services)-1
	$states[$i] = (StringCompare(_ServiceStatus($services[$i]),"Running",2)==0)
	_DebugOut($services[$i]&": "&$states[$i])
	$needStartStop = $needStartStop or not $states[$i]
Next

If $needStartStop Then
	If MsgBox(36, $title, "Alguns serviços necessários não estão rodando. Deseja iniciá-los?")==6 Then
		ProgressOn($title,"Iniciando serviços...")
		$step = 100/UBound($services)
		For $i = 0 To UBound($services)-1
			If $states[$i] Then
				ProgressSet($step*$i)
				ContinueLoop
			EndIf
			ProgressSet($step*($i+1),$services[$i]&"...")
			_ServiceStart($services[$i])
			$states[$i] = true
		Next
		ProgressSet(100," ","Concluido!")
		Sleep(500)
		ProgressOff()
	Else
		$needStartStop = false
	EndIf
Else
	$needStartStop = true ; prepares to stop
EndIf

RunWait($app, @ScriptDir)

If $needStartStop Then
	If MsgBox(36, $title, "Deseja finalizar os serviços utilizados?")==6 Then
		ProgressOn($title,"Parando serviços...")
		$step = 100/UBound($services)
		For $i = 0 To UBound($services)-1
			If not $states[$i] Then
				ProgressSet($step*$i)
				ContinueLoop
			EndIf
			ProgressSet($step*($i+1),$services[$i]&"...")
			_ServiceStop($services[$i])
		Next
		ProgressSet(100," ","Concluido!")
		Sleep(500)
		ProgressOff()
	EndIf
EndIf

Func _ServiceStatus($s_ServiceName)
    Local Const $wbemFlagReturnImmediately = 0x10
    Local Const $wbemFlagForwardOnly = 0x20
    Local $colItems = "", $objItem
    Local $objWMIService = ObjGet("winmgmts:\\" & @ComputerName & "\root\CIMV2")
    If @error Then
        MsgBox(16, "_RetrieveServiceState", "ObjGet Error: winmgmts")
        Return
    EndIf
    $colItems = $objWMIService.ExecQuery ("SELECT State FROM Win32_Service WHERE Name = '" & $s_ServiceName & "'", "WQL", _
            $wbemFlagReturnImmediately + $wbemFlagForwardOnly)
    If @error Then
        MsgBox(16, "_RetrieveServiceState", "ExecQuery Error: SELECT State FROM Win32_Service")
        Return
    EndIf
    If IsObj($colItems) Then
        For $objItem In $colItems
			_DebugOut($s_ServiceName&": "&$objItem.State)
            Return $objItem.State
        Next
    EndIf
EndFunc   ;==>_RetrieveServiceState
Func _ServiceStart($s_ServiceName)
	RunWait(@ComSpec & " /c " & "net start " & $s_ServiceName, "", @SW_HIDE)
	If @error Then
		MsgBox(48, $title, "Não foi possível iniciar o serviço '"&$s_ServiceName&"'.")
	EndIf
EndFunc
Func _ServiceStop($s_ServiceName)
	RunWait(@ComSpec & " /c " & "net stop " & $s_ServiceName, "", @SW_HIDE)
	If @error Then
		MsgBox(48, $title, "Não foi possível parar o serviço '"&$s_ServiceName&"'.")
	EndIf
EndFunc