#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=.\Bulk File UAC Fix.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Fix para os problemas de escrita na pasta de instalação do CW em versões do Windows com UAC.
#AutoIt3Wrapper_Res_Description=Bulk File UAC Fix
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2014 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Citadon CW UAC Fix
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
Dim $title = "Bulk File UAC Fix"
Local $Wow64 = ""
ProgressOn($title, "Checking administrative privileges...")
If Not IsAdmin() Then
	MsgBox(16, $title, "This fix must be run with administrative privileges!")
	Exit
EndIf
ProgressSet(20, "Checking platform...")
If @AutoItX64 Then $Wow64 = "\Wow6432Node"
ProgressSet(40, "Getting info from registry...")
Local $path = RegRead("HKLM\SOFTWARE" & $Wow64 & "\Microsoft\Windows\CurrentVersion\Uninstall\{0BEEBA02-9256-45D7-97B6-E1FB203DD83F}", "InstallLocation")
ProgressSet(60, "Checking install path...")
If Not FileExists($path & "BulkfileUpload.exe") Then
	MsgBox(16, $title, "Corrupt installation! Please reinstall Bulk File!")
	Exit
EndIf
ProgressSet(80, "Applying fix...")
RunWait(@ComSpec & " /c " & "icacls " & $path & " /grant *S-1-5-32-545:(CI)(OI)M /T /L", "", @SW_HIDE)
ProgressSet(100, "Done!")
Sleep(500)
ProgressOff()
MsgBox(0, $title, "Fix successfuly applied!")
