#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=WinAPI.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Comandos utilitários baseados na API do Windows.
#AutoIt3Wrapper_Res_Description=WinAPI
#AutoIt3Wrapper_Res_Fileversion=1.1.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2017 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|WinAPI
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <Constants.au3>
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <Array.au3>
#include <WindowsConstants.au3>
#include <WinAPI.au3>
;#include <WinAPISys.au3>
;#include <WinAPIProc.au3>

#Region Tray Icon Settings
Opt("TrayAutoPause", 0)
Opt("TrayMenuMode", 3)

Local $title = "Windows API"
TraySetIcon("WinAPI.ico", 0)
TrayCreateItem($title)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("")
$funcSubMenu = TrayCreateMenu("Functions")
TrayCreateItem("Ctrl+F1: Kill Process(es)", $funcSubMenu)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("Ctrl+F9: Window Style: visible and non-tool", $funcSubMenu)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("Ctrl+F10: Toggle Foreground Window Border", $funcSubMenu)
TrayItemSetState(-1, $TRAY_DISABLE)
TrayCreateItem("Ctrl+F12: Close Windows API", $funcSubMenu)
TrayItemSetState(-1, $TRAY_DISABLE)
Local $mySubMenu = TrayCreateMenu("About")
Local $siteItem = TrayCreateItem("My Blog", $mySubMenu)
Local $projItem = TrayCreateItem("My Projects", $mySubMenu)
Local $emailItem = TrayCreateItem("Email me", $mySubMenu)
TrayCreateItem("")
Local $exitItem = TrayCreateItem("Exit")
TraySetToolTip($title)
#EndRegion

#Region Internal Functions
Func ToggleWindowBorder($hWnd)
	$lStyle = _WinAPI_GetWindowLong($hWnd, $GWL_STYLE)
	_WinAPI_SetWindowLong($hWnd, $GWL_STYLE, BitXOR($lStyle, $WS_CAPTION, $WS_THICKFRAME, $WS_SYSMENU))

	$lExStyle = _WinAPI_GetWindowLong($hWnd, $GWL_EXSTYLE)
	_WinAPI_SetWindowLong($hWnd, $GWL_EXSTYLE, BitXOR($lExStyle, $WS_EX_DLGMODALFRAME, $WS_EX_CLIENTEDGE, $WS_EX_STATICEDGE))
EndFunc
#EndRegion

#Region Hotkey Functions
func TaskKill()
	Local $pList = ProcessList()

	Local $aList[0]
	For $iProcess = 1 To $pList[0][0]
		If $pList[$iProcess][1] > 10 Then
			_ArrayAdd($aList, StringFormat("%s (%d)", $pList[$iProcess][0], $pList[$iProcess][1]))
		EndIf
	Next

	#Region ### START Koda GUI section ### Form=
	$killProcessForm = GUICreate("Kill Process", 226, 339, 192, 124, BitOR($WS_POPUP, $WS_CAPTION), $WS_EX_TOPMOST)
	$listProcess = GUICtrlCreateList("", 8, 8, 209, 266, BitOR($GUI_SS_DEFAULT_LIST,$LBS_MULTIPLESEL))
	GUICtrlSetData(-1, _ArrayToString($aList))
	$cbTreeKill = GUICtrlCreateCheckbox("Tree Kill", 8, 280, 97, 17)
	$btnOk = GUICtrlCreateButton("OK", 8, 304, 99, 25)
	GUICtrlSetStyle(-1, $BS_DEFPUSHBUTTON)
	GUICtrlSetState(-1, $GUI_DEFBUTTON)
	$btnCancel = GUICtrlCreateButton("Cancel", 120, 304, 99, 25)
	GUISetState(@SW_SHOW)
	#EndRegion ### END Koda GUI section ###

	While 1
		$nMsg = GUIGetMsg()
		Switch $nMsg
			Case $btnOk
				GUISetState(@SW_HIDE)

				Local $aList = _GUICtrlListBox_GetSelItemsText($listProcess)
				Local $killList[$aList[0]]
				for $i = 1 to $aList[0]
					$procInfo = StringSplit(StringReplace($aList[$i], ")", ""), " (", $STR_ENTIRESPLIT)
					$killList[$i-1] = "/PID " & $procInfo[2]
				Next

				If GUICtrlRead($cbTreeKill) == $GUI_CHECKED Then
					_ArrayInsert($killList, 0, "/T")
				EndIf

				;MsgBox(0, "Debug", @ComSpec & " /c  taskkill /F " & _ArrayToString($killList, " "))

				Run(@ComSpec & " /c  taskkill /F " & _ArrayToString($killList, " "), "", @SW_HIDE)

				ExitLoop
			Case $GUI_EVENT_CLOSE, $btnCancel
				GUISetState(@SW_HIDE)
				ExitLoop
		EndSwitch
	WEnd
	GUIDelete($killProcessForm)

	;Local $proc = InputBox("Task Kill", "Specify active process to kill all instances (single kill):")
	;If @error = 0 Then
	;	Run(@ComSpec & " /c  taskkill /F /IM " & $proc, "", @SW_HIDE)
	;EndIf
EndFunc

Func ToggleForegroundWindowBorder()
	TrayTip("Windows API", "Changing active application border...", 1, $TIP_ICONASTERISK)
	ToggleWindowBorder(_WinAPI_GetForegroundWindow());
EndFunc

Func MakeTiledWindow()
   TrayTip("Windows API", "Changing active application style...", 1, $TIP_ICONASTERISK)
   $hWnd = _WinAPI_GetForegroundWindow()

   $lStyle = _WinAPI_GetWindowLong($hWnd, $GWL_STYLE)
   $lStyle = BitOR(BitAND($lStyle, BitNOT($WS_POPUP)), $WS_VISIBLE) ; Force visible and disable popup
   _WinAPI_SetWindowLong($hWnd, $GWL_STYLE, $lStyle)

   $lExStyle = _WinAPI_GetWindowLong($hWnd, $GWL_EXSTYLE)
   $lExStyle = BitOR(BitAND($lExStyle, BitNOT($WS_EX_TOOLWINDOW)), $WS_EX_WINDOWEDGE, $WS_EX_APPWINDOW) ; Add window edge, app window (force on task bar) and removes tool window flag
   _WinAPI_SetWindowLong($hWnd, $GWL_EXSTYLE, $lExStyle)
EndFunc

Func ExitScript()
	TrayTip("Windows API", "Exiting!", 1, $TIP_ICONASTERISK)
	Sleep(1000)
	Exit
EndFunc
#EndRegion

#Region Set Hotkeys
HotKeySet("^{F1}", "TaskKill")
HotKeySet("^{F9}", "MakeTiledWindow")
HotKeySet("^{F10}", "ToggleForegroundWindowBorder")
HotKeySet("^{F12}", "ExitScript")
#EndRegion

; Loop script indefenitely
While 1
	Switch TrayGetMsg()
		Case $siteItem
			ShellExecute("http://william.wmoraes.nom.br/")
		Case $projItem
			ShellExecute("http://git.william.moraes.nom.br/")
		Case $emailItem
			ShellExecute("http://scr.im/wwmoraes")
		Case $exitItem
			ExitScript()
	EndSwitch
WEnd