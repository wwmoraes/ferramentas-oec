#RequireAdmin
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=.\settings.ico
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_UseUpx=y
#AutoIt3Wrapper_Res_Comment=Ajusta os sites disponíveis para conexão no Citadon CW, listando os 4 servidores disponíveis (US, UK, EA, CDN).
#AutoIt3Wrapper_Res_Description=CW Site Preferences Setup
#AutoIt3Wrapper_Res_Fileversion=1.0.0.0
#AutoIt3Wrapper_Res_Fileversion_AutoIncrement=p
#AutoIt3Wrapper_Res_LegalCopyright=Copyleft 2017 William Moraes
#AutoIt3Wrapper_Res_Language=1046
#AutoIt3Wrapper_Res_requestedExecutionLevel=requireAdministrator
#AutoIt3Wrapper_Res_Field=CompanyName|Construtora Norberto Odebrecht S/A
#AutoIt3Wrapper_Res_Field=ProductName|Citadon CW Site Preferences Setup
#AutoIt3Wrapper_Res_Field=OriginalFilename|%scriptfile%.exe
#AutoIt3Wrapper_Res_Field=HomePage|http://www.odebrecht.com/
#AutoIt3Wrapper_Run_Before=del "%scriptdir%\..\%scriptfile%*.exe"
#AutoIt3Wrapper_Run_After=move "%out%" "%scriptdir%\..\%scriptfile%.exe"
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <MsgBoxConstants.au3>
#include <FileConstants.au3>

If StringInStr(@OSArch, "64") Then
	$installDir = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\CITADON\ProjectNet\Client", "AppInstallDir")
Else
	$installDir = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\CITADON\ProjectNet\Client", "AppInstallDir")
EndIf


FileInstall(".\sitePreferences.xml", $installDir, $FC_OVERWRITE)
MsgBox($MB_ICONINFORMATION, "Citadon CW Site Preferences Setup", "Instalação concluída.")