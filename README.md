Leia-me!
=======
Este repositório agrega diversas ferramentas para facilitar algumas atividades repetitivas e/ou morosas no meu trabalho, e de alguns colegas.

Ferramentas
---------------
Aqui vai a lista, com a descrição e o link direto para o download da última versão.

> **UPK Presenter** ([Download](http://git.wwmoraes.com/work-helper-tools/downloads/UPK%20Presenter.exe))
> 
> Permite utilizar apontadores com as apresentações em UPK
> - Navega nas pastas
> - Entra e sai em tópicos
> - Avança e retorna nas páginas do tópico

----------

Outros downloads [aqui](http://git.wwmoraes.com/work-helper-tools/downloads).

TODO: Fazer os notes para os outros downloads.